#!/usr/bin/python

import sys, getopt

from collections import Iterator
is_iterator = lambda x: isinstance(x, Iterator)

def skip(it, n):
	for i in xrange(n): next(it, None)

class ObCodeError(Exception): pass

class Ob(object):
	def __init__(self, children=(), line=None, flat=None): # Be sure not to pass a wrong flat value
		if isinstance(children, type(self)):
			self.children = children.children
			self.flat = children.flat
			if line is None: self.line = children.line
			else: self.line = line
		else:
			self.children = tuple(children)
			self.line = line
			if flat: self.flat = flat
			else:
				self.flat = []
				for ob in self.children:
					self.flat.append(len(ob))
					self.flat.extend(ob.flat)
	def __hash__(self): return hash(tuple(self.flat))
	def __eq__(lhs, rhs): return type(lhs) == type(rhs) and lhs.flat == rhs.flat
	def __ne__(lhs, rhs): return not (lhs == rhs)
	def __str__(self): return '('+''.join(str(sub) for sub in self.children)+')'
	def __repr__(self): return "parse(%r)" % str(self)
	def __len__(self): return len(self.children)
	def __iter__(self): return iter(self.children)
	def __getitem__(self, i): return self.children[i]
	def __add__(lhs, rhs): return Ob(lhs.children+rhs.children, flat=lhs.flat+rhs.flat)
	def lineno(self):return "" if self.line is None else " (line %d)"%self.line

def is_packed(code):
	return code and ord(code[0]) & 0x80

def parse(code, wimp=False):
	if is_packed(code): return unpack(code)
	ob_stack = []
	curr_ob = []
	line = 1
	enum = enumerate(code)
	for i, char in enum:
		if char == '(':
			ob_stack.append(curr_ob)
			curr_ob = []
		elif char == ')':
			if ob_stack:
				ob = Ob(curr_ob, line)
				curr_ob = ob_stack.pop()
				curr_ob.append(ob)
			else: raise ObCodeError("Unbalanced brackets (line %d)" % line)
		elif char == '\n':
			line += 1
		elif wimp:
			if code[i:i+3] == 'NOP':
				curr_ob.append(Ob(NOP, line))
				skip(enum, 3)
			elif code[i:i+4] == 'PUSH':
				curr_ob.append(Ob(PUSH, line))
				skip(enum, 4)
			elif code[i:i+5] == 'STORE':
				curr_ob.append(Ob(STORE, line))
				skip(enum, 5)
			elif code[i:i+4] == 'LOAD':
				curr_ob.append(Ob(LOAD, line))
				skip(enum, 4)
			elif code[i:i+5] == 'WHILE':
				curr_ob.append(Ob(WHILE, line))
				skip(enum, 5)
			elif code[i:i+5] == 'USING':
				curr_ob.append(Ob(USING, line))
				skip(enum, 5)
			elif code[i:i+3] == 'END':
				curr_ob.append(Ob(END, line))
				skip(enum, 3)
			elif code[i:i+4] == 'SWAP':
				curr_ob.append(Ob(SWAP, line))
				skip(enum, 4)
			elif code[i:i+3] == 'OUT':
				curr_ob.append(Ob(OUT, line))
				skip(enum, 3)
			elif code[i:i+2] == 'IN':
				curr_ob.append(Ob(IN, line))
				skip(enum, 2)
			elif code[i:i+3] == 'CAT':
				curr_ob.append(Ob(CAT, line))
				skip(enum, 3)
			elif code[i:i+3] == 'DEF':
				curr_ob.append(Ob(DEF, line))
				skip(enum, 3)
			elif code[i:i+3] == 'DUP':
				curr_ob.extend((Ob(STORE,line), Ob(LOAD,line), Ob(LOAD,line)))
				skip(enum, 3)
	if ob_stack: raise ObCodeError("Unbalanced brackets (EOF)")
	if len(curr_ob) != 1: raise ObCodeError("Didn't find exactly one object")
	return curr_ob[0]

NOP = parse('()')
PUSH = parse('(())')
STORE = parse('(()())')
LOAD = parse('((())())')
WHILE = parse('(()(()))')
USING = parse('((())(()))')
END = parse('((()))')
SWAP = parse('((()()))')
OUT = parse('(()()())')
IN = parse('((())()())')
CAT = parse('(()()(()))')
DEF = parse('((()())())')

def pack(ob, debug=lambda x:None):
	if len(ob) < 2: raise ObCodeError("Program %s containing less than two instructions can't be compressed" % ob)
	big = pack_to_num(ob, 0L)
	while big & 1: big >>= 1
	big >>= 1
	debug("Program number is %x" % big)
	result = ""
	while big:
		char = big & 0xff
		big >>= 8
		if big: result = chr(char)+result
		elif char & 0x80: result = '\x80'+chr(char)+result
		else: result = chr(char | 0x80)+result
	return result

def pack_to_num(ob, num):
	for i in ob:
		num = pack_to_num(i, num << 1) << 1 | 1
	return num

def unpack(code):
	ob_stack = []
	curr_ob = []
	code = [ord(char) for char in code]
	code[0] &= 0x7f
	had_one = 0
	for char in code:
		for i in reversed(range(8)):
			bit = char >> i & 1
			if bit:
				had_one = 1
				if ob_stack:
					ob = Ob(curr_ob)
					curr_ob = ob_stack.pop()
					curr_ob.append(ob)
				else: curr_ob = [Ob(curr_ob)]
			elif had_one:
				ob_stack.append(curr_ob)
				curr_ob = []
	curr_ob.append(Ob())
	while ob_stack:
		ob = Ob(curr_ob)
		curr_ob = ob_stack.pop()
		curr_ob.append(ob)
	return Ob(curr_ob)

def execute(code, input_data='', out_fun = lambda x: None, debug = lambda x: None):
	if isinstance(code, basestring): code = parse(code)
	elif not isinstance(code, Ob): raise TypeError("Unknown type %r as code" % type(code).__name__)

	if isinstance(input_data, basestring) or is_iterator(input_data):
		def input_gen():
			for char in input_data: yield char
			while True: yield 0
	elif callable(input_data):
		def input_gen():
			while True:
				char = input_data()
				if char: yield char
				else: yield 0
	else: raise TypeError("Bad type %r as input, use strings, iterators, or functions" % type(input_data).__name__)

	if not callable(out_fun): raise TypeError("Bad type %r as output function, use callables" % type(out_fun).__name__)

	if not callable(debug): raise TypeError("Bad type %r as debug function, use callables" % type(debug).__name__)

	return execute_object(code, input_gen(), out_fun, debug, "", [], [], {}, [])[0]

def execute_object(code, input_gen, out_fun, debug, output, stacks, curr_stack, subs, register):
	it = iter(code)
	for ob in it:
		try: output, stacks, curr_stack, subs, register = \
		  execute_object(subs[ob], input_gen, out_fun, debug, output, stacks, curr_stack, subs, register)
		except KeyError:
			if ob == PUSH:
				try:
					curr_stack.append(next(it))
					debug("PUSH %s" % curr_stack[-1])
				except StopIteration: raise ObCodeError("%s (PUSH) cannot occur as last instruction in object%s" % (ob, ob.lineno()))
			elif ob == STORE:
				stack_depth(curr_stack, ob)
				register = curr_stack.pop()
				debug("STORE %s" % register)
			elif ob == LOAD:
				debug("LOAD %s" % register)
				curr_stack.append(register)
			elif ob == WHILE:
				stack_depth(curr_stack, ob)
				ob = curr_stack.pop()
				debug("WHILE %s"%ob)
				while curr_stack:
					output, stacks, curr_stack, subs, register = execute_object(ob, input_gen, out_fun, debug, output, stacks, curr_stack, subs, register)
				debug("DONE")
			elif ob == USING:
				stack_depth(curr_stack, ob)
				stack = list(curr_stack.pop())
				stacks.append(curr_stack)
				curr_stack = stack
				debug("USING [%s]" % ', '.join(str(ob) for ob in stack))
			elif ob == END:
				debug("END [%s]" % ', '.join(str(ob) for ob in curr_stack))
				ob = Ob(curr_stack)
				if stacks:
					curr_stack = stacks.pop()
					curr_stack.append(ob)
				else: curr_stack = [ob]
			elif ob == SWAP:
				stack_depth(curr_stack, ob, 2)
				ob = curr_stack.pop()
				ob2 = curr_stack.pop()
				debug("SWAP %s, %s" % (ob2,ob))
				curr_stack.append(ob)
				curr_stack.append(ob2)
			elif ob == OUT:
				stack_depth(curr_stack, ob)
				ob = curr_stack.pop()
				char = len(ob) % 0xff
				debug("OUT %d [%s]"%(char,ob))
				char = chr(char)
				out_fun(char)
				output += char
			elif ob == IN:
				debug("IN ?")
				char = next(input_gen)
				if isinstance(char, basestring): char = ord(char)
				ob = Ob(Ob() for i in range(char))
				debug("IN %d [%s]"%(char,ob))
				curr_stack.append(Ob(Ob() for i in range(char)))
			elif ob == CAT:
				stack_depth(curr_stack, ob, 2)
				ob2 = curr_stack.pop()
				ob = curr_stack.pop()
				debug("CAT %s, %s"%(ob,ob2))
				curr_stack.append(ob+ob2)
			elif ob == DEF:
				stack_depth(curr_stack, ob, 2)
				code = curr_stack.pop()
				name = curr_stack.pop()
				subs[name] = code
				debug("DEF %s, %s"%(name,code))
			elif ob != NOP: raise ObCodeError("Unknown instruction: %s%s" % (ob, ob.lineno()))
	return output, stacks, curr_stack, subs, register

def stack_depth(stack, ob, req=1):
	if req > len(stack): raise ObCodeError("Stack underflow%s" % ob.lineno())

if __name__ == '__main__':
	def usage():
		print >> sys.stderr, """\
Synopsis: %s [-pdwWh] [-e <program> | <file>]

Options:
	-p, --pack
		Pack/unpack the code and print to STDOUT.
	-d, --debug
		Print debugging information while executing
	-w, --wimp, --wimpy
		Replace names of instructions by the correct instructions at
		parse time and add the DUP keyword which equals STORE LOAD LOAD
	-W, --unwimp
		Output a golfed non-wimpy version of a wimpy program. Can also
		be used for golfing normal programs if they don't contain
		instruction aliases.
	-e <program>
		Use the given program instead of file/STDIN.
	-h, -?, --help
		Print this help and exit""" % sys.argv[0]
		sys.exit()

	try:
		try: opts, args = getopt.getopt(sys.argv[1:], 'dpwWe:h?', ['help', 'wimp', 'wimpy', 'unwimp', 'pack', 'debug'])
		except getopt.GetoptError: usage()
		code = None
		do_pack = False
		debug = lambda x: None
		wimp = False
		unwimp = False
		for opt, val in opts:
			if opt == '-p' or opt == '--pack': do_pack = True
			elif opt == '-d' or opt == '--debug': debug = lambda x: sys.stderr.write(x+'\n')
			elif opt == '-w' or opt == '--wimp' or opt == '--wimpy': wimp = True
			elif opt == '-W' or opt == '--unwimp':
				wimp = True
				unwimp = True
			elif opt == '-e': code = val
			elif opt == '-h' or opt == '-?' or opt == '--help': usage()
		if code is None:
			if args and args[0] != '-':
				try:
					with open(args[0]) as f: code = f.read()
				except EnvironmentError as err:
					print >> sys.stderr, 'Error:', err.strerror
					sys.exit(1)
			else: code = sys.stdin.read()
		if do_pack:
			if is_packed(code): print unpack(code)
			else:
				code = pack(parse(code, wimp), debug)
				print >> sys.stderr, "Packed: %d bytes" % len(code)
				sys.stdout.write(code)
		elif unwimp:
			if is_packed(code):
				print >> sys.stderr, "Cannot unwimp packed program"
				sys.exit(1)
			code = str(parse(code, True))
			print >> sys.stderr, "Size: %d bytes" % len(code)
			print code
		else: execute(parse(code, wimp), lambda: sys.stdin.read(1), lambda x: (sys.stdout.write(x), sys.stdout.flush()), debug)
	except ObCodeError as err: print >> sys.stderr, err
	except KeyboardInterrupt: print >> sys.stderr, "Interrupted"
