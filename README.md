# [ObCode](https://esolangs.org/wiki/ObCode)

ObCode is a Turing-Tarpit based on objects. Each object contains zero or more
other objects.

## Syntax

    obj ::= '(' obj_list ')'
    obj_list ::= obj_list obj |

Each object is a pair of parentheses with the other objects inbetween. Every
program is an object. All characters other than parentheses are ignored.

## Memory model

ObCode uses an unbounded stack of stacks as memory, and a single register.

## Execution model

If an object is executed, each sub-object interpreted as an instruction.

|Instruction |Alias  |Action|
|------------|-------|------|
|`()`        |NOP    |Does nothing|
|`(())`      |PUSH   |Push the next object|
|`(()())`    |STORE  |Pop an object from the current stack and store it in the register.|
|`((())())`  |LOAD   |Push the object currently held in the register.|
|`(()(()))`  |WHILE  |Pop an object and execute it as code until the current stack is empty.|
|`((())(()))`|USING  |Pop an object from the current stack and push the object as a new stack.|
|`((()))`    |END    |Pop the entire current stack as an object and push it onto the stack underneath it.|
|`((()()))`  |SWAP   |Swap the top two objects on the current stack.|
|`(()()())`  |OUT    |Pop an object, take the length, and print the ASCII character corresponding to that number.|
|`((())()())`|IN     |Read a character from input and push an object containing as many empty objects as the ASCII value of that character.|
|`(()()(()))`|CAT    |Pop two objects and concatenate them.|
|`((()())())`|DEF    |Pop a, pop b. Redefine b as an instruction that does a.|

## Packed ObCode

Packed ObCode is a more efficient variant of [Binary ObCode](https://esolangs.org/wiki/ObCode#Binary_ObCode).
The interpreter does automatically detect it. You can pack/unpack programs with
the `-p` command line option.

## Wimp mode

Wimp mode can be activated with the `-w` switch. With wimp mode, the instruction
aliases are replaced by their objects, and `DUP` is replaced with
`STORE LOAD LOAD` (warning: this will change the register!). You can convert
wimp mode programs to normal mode with the `-W` switch.

## Debugging

The `-d` command line option activates debugging mode. It writes large amounts
of debugging info to STDERR.

## Computational class

ObCode is Turing-complete, as every brainfuck program can be translated to ObCode:

brainfuck|ObCode|
---|---
BEGIN   |`( (())() (())(()) ((())(()))`
`+`     |`(())(()) (()()(()))`
`-`     |`((())(())) (()()) ((()))`
`.`     |`(()()) ((())()) ((())()) (()()())`
`,`     |`(()()) ((())()())`
`<`     |`((())) ((()())) (())(()) ((()())) (()()(())) ((())(())) (()()) ((())) ((()())) ((())(())) ((())())`
`>`     |`(()()) ((())) ((()())) ((())(())) ((())()) ((())) ((()())) (())(()) ((()())) (()()(())) ((())(()))`
`[`     |`((())(())) (())( ((()))`
`]`     |`((())(())) )(()(())) ((()))`
END     |`)`
